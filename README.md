# Image_Classification

In this image classification experiment, various images are each coverted into four different vectorization models (Autoencoder, Singular Value Decomposition, RGB Histograms, and HSV Histograms). Image classification algorithm is then run on all four models to predict the most likely type for each image. The predictive performance pertaining to each model is compared using two different metrics (Euclidean and Pearson), and the results are output in the form of confusion matrices.

To run, open the .ipynb file in iPython Notebook.
